using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TodoApi.Models;

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToDoController : ControllerBase
    {
        private readonly TodoContext _context;

        public ToDoController(TodoContext context)
        {
            _context = context;
        }
        
        [HttpGet]
        public ActionResult Get()
        {
            List<TodoItem> items = _context.TodoItems.ToList();
            return Ok(items);
        }
        
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            TodoItem item = _context.TodoItems.FirstOrDefault(i => i.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }
        
        [HttpPost]
        public ActionResult Save([FromBody] TodoItem item)
        {
            if (item == null)
            {
                return NotFound();
            }
            _context.TodoItems.Add(item);
            _context.SaveChanges();
            
            return Ok(item);
        }
        
        [HttpPut]
        public ActionResult Update([FromBody] TodoItem item)
        {
            if (item == null)
            {
                return NotFound();   
            }
            
            _context.TodoItems.Update(item);
            _context.SaveChanges();
            return NoContent();
        }
        
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            TodoItem item = _context.TodoItems.FirstOrDefault(i => i.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            
            _context.TodoItems.Remove(item);
            _context.SaveChanges();
            return NoContent();
        }
    }
}